package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"sync/atomic"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go"
)

type Publisher interface {
	Publish(subject string, data []byte) error
}

type JetStreamPublisher struct {
	js nats.JetStream
}

func (p *JetStreamPublisher) Publish(subject string, data []byte) error {
	if _, err := p.js.Publish(subject, data); err != nil {
		return err
	}
	return nil
}

type Subscriber interface {
	ChanSubscribe(subject string, ch chan *nats.Msg) (*nats.Subscription, error)
	Reconnect(ctx context.Context) error
}

type CoreSubscriber struct {
	nc *nats.Conn
}

func (s *CoreSubscriber) ChanSubscribe(subject string, ch chan *nats.Msg) (*nats.Subscription, error) {
	return s.nc.ChanSubscribe(subject, ch)
}

func (s *CoreSubscriber) Reconnect(ctx context.Context) error {
	url := s.nc.ConnectedUrl()
	s.nc.Close()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-time.After(reconnectTime):
	}
	nc, err := nats.Connect(url)
	if err != nil {
		return err
	}
	s.nc = nc
	return nil
}

type JetStreamSubscriber struct {
	nc  *nats.Conn
	js  nats.JetStream
	sub *nats.Subscription
}

func (s *JetStreamSubscriber) ChanSubscribe(subject string, ch chan *nats.Msg) (*nats.Subscription, error) {
	consumerName := strings.TrimPrefix(subject, "foo.")
	sub, err := s.js.ChanSubscribe(subject, ch, nats.Durable(consumerName), nats.AckWait(50*time.Millisecond))
	if err != nil {
		return nil, err
	}
	s.sub = sub
	return sub, nil
}

func (s *JetStreamSubscriber) Reconnect(ctx context.Context) error {
	url := s.nc.ConnectedUrl()
	s.nc.Close()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-time.After(reconnectTime):
	}
	nc, err := nats.Connect(url)
	if err != nil {
		return err
	}
	s.nc = nc
	j, err := nc.JetStream()
	if err != nil {
		return err
	}
	s.js = j
	return nil
}

const (
	natsCoreURL      = "nats://localhost:4222"
	natsJetStreamURL = "nats://localhost:5222"
	reconnectTime    = 20 * time.Millisecond
)

func TestMessageLoss(t *testing.T) {
	prepareStream(t)
	numMessages := 100
	log.Printf("numMessage: %d, reconnectTime: %v", numMessages, reconnectTime)

	testCases := []struct {
		name        string
		isJetStream bool
		testFunc    func(ctx context.Context, sub Subscriber, subject string) <-chan *nats.Msg
	}{
		{"NATS Core", false, subscribeMessages},
		{"NATS Core with reconnect", false, subscribeMessagesWithReconnect},
		{"NATS JetStream", true, subscribeMessages},
		{"NATS JetStream with reconnect", true, subscribeMessagesWithReconnect},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			subject := fmt.Sprintf("foo.%s", uuid.Must(uuid.NewRandom()))
			log.Printf("subject: %s", subject)
			sub, err := getSubscriber(tc.isJetStream)
			if err != nil {
				t.Fatalf("[%s] failed to get subscriber: %+v", tc.name, err)
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			recvCh := tc.testFunc(ctx, sub, subject)
			recvCnt := uint32(0)
			go func() {
				for {
					select {
					case <-ctx.Done():
						return
					case msg := <-recvCh:
						// Do nothing without JetStream
						if err := msg.Ack(); err != nil && !errors.Is(err, nats.ErrMsgNoReply) {
							log.Printf("failed to ACK: %+v", err)
							continue
						}
						log.Printf("received: %s", string(msg.Data))
						atomic.AddUint32(&recvCnt, 1)
					}
				}
			}()
			pub, err := getPublisher(tc.isJetStream)
			if err != nil {
				log.Fatalf("failed to connect to NATS: %+v", err)
			}
			publishMessages(pub, subject, numMessages)
			numReceived := int(atomic.LoadUint32(&recvCnt))
			log.Printf("[%s] message received: %d/%d (loss: %.2f%%)", tc.name, numReceived, numMessages,
				(1.0-(float64(numReceived)/float64(numMessages)))*100)
		})
	}
}

func prepareStream(t *testing.T) {
	nc, err := nats.Connect(natsJetStreamURL)
	if err != nil {
		t.Fatalf("failed to connect to NATS: %+v", err)
	}
	js, err := nc.JetStream()
	if err != nil {
		t.Fatalf("failed to get jetstream: %+v", err)
	}
	if _, err := js.AddStream(&nats.StreamConfig{
		Name:     "MESSAGES",
		Subjects: []string{"foo.*"},
	}); err != nil {
		t.Fatalf("failed to add stream: %+v", err)
	}
	t.Cleanup(func() {
		_ = js.DeleteStream("MESSAGES")
	})
}

func getPublisher(isJetStream bool) (Publisher, error) {
	url := natsCoreURL
	if isJetStream {
		url = natsJetStreamURL
	}
	nc, err := nats.Connect(url)
	if err != nil {
		return nil, err
	}
	if isJetStream {
		js, err := nc.JetStream()
		if err != nil {
			return nil, err
		}
		return &JetStreamPublisher{js: js}, nil
	}
	return nc, nil
}

func getSubscriber(isJetStream bool) (Subscriber, error) {
	url := natsCoreURL
	if isJetStream {
		url = natsJetStreamURL
	}
	nc, err := nats.Connect(url)
	if err != nil {
		return nil, err
	}
	if isJetStream {
		js, err := nc.JetStream()
		if err != nil {
			return nil, err
		}
		return &JetStreamSubscriber{nc: nc, js: js}, nil
	}
	return &CoreSubscriber{nc: nc}, nil
}

func publishMessages(pub Publisher, subject string, numMessages int) {
	interval := 30 * time.Millisecond
	for i := 0; i < numMessages; i++ {
		if err := pub.Publish(subject, []byte(fmt.Sprintf("hello-%d", i))); err != nil {
			log.Printf("failed to publish: %+v", err)
		}
		time.Sleep(interval)
	}
	time.Sleep(1 * time.Second)
}

func subscribeMessages(ctx context.Context, sub Subscriber, subject string) <-chan *nats.Msg {
	recvCh := make(chan *nats.Msg, 256)
	if _, err := sub.ChanSubscribe(subject, recvCh); err != nil {
		log.Fatalf("failed to subscribe %s: %+v", subject, err)
	}
	return recvCh
}

func subscribeMessagesWithReconnect(ctx context.Context, sub Subscriber, subject string) <-chan *nats.Msg {
	reconnectInterval := 250 * time.Millisecond
	recvCh := make(chan *nats.Msg, 256)

	reconnect := func() {
		if err := sub.Reconnect(ctx); err != nil {
			if ctx.Err() != nil {
				return
			}
			log.Fatalf("failed to reconnect to NATS: %+v", err)
		}
		if _, err := sub.ChanSubscribe(subject, recvCh); err != nil {
			log.Fatalf("failed to subscribe %s: %+v", subject, err)
		}
		log.Printf("--- reconnected ---")
	}
	reconnect()

	go func() {
		ticker := time.NewTicker(reconnectInterval)
		defer ticker.Stop()
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				reconnect()
			}
		}
	}()
	return recvCh
}
