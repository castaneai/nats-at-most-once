NATS (Core, not JetStream) has a messaging semantics of at-most-once.
This strategy allows for message loss.
If the consumer (subscriber) reconnects too often, some messages will be lost.

On the other hand, NATS JetStream almost guarantees at-least-once delivery.
Thus, message delivery is more reliable even when many consumer reconnection occur.

Not 100% even with JetStream? It's probably failing to send an ACK!
Since this test is short, the test finishes before the message is retransmitted, and the loss rate is calculated early.

```sh
$ docker-compose up -d
$ go test -v .

=== RUN   TestMessageLoss
2022/02/11 04:32:33 numMessage: 100, reconnectTime: 20ms
=== RUN   TestMessageLoss/NATS_Core
...
2022/02/11 04:32:40 [NATS Core] message received: 100/100 (loss: 0.00%)
=== RUN   TestMessageLoss/NATS_Core_with_reconnect
...
2022/02/11 04:32:47 [NATS Core with reconnect] message received: 74/100 (loss: 26.00%)
=== RUN   TestMessageLoss/NATS_JetStream
...
2022/02/11 04:32:54 [NATS JetStream] message received: 100/100 (loss: 0.00%)
=== RUN   TestMessageLoss/NATS_JetStream_with_reconnect
...

2022/02/11 04:33:01 [NATS JetStream with reconnect] message received: 100/100 (loss: 0.00%)
--- PASS: TestMessageLoss (28.20s)
    --- PASS: TestMessageLoss/NATS_Core (7.04s)
    --- PASS: TestMessageLoss/NATS_Core_with_reconnect (7.05s)
    --- PASS: TestMessageLoss/NATS_JetStream (7.05s)
    --- PASS: TestMessageLoss/NATS_JetStream_with_reconnect (7.06s)
PASS
ok  	github.com/castaneai/nats-at-most-once	28.202s
```
