module github.com/castaneai/nats-at-most-once

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0
	github.com/nats-io/nats-server/v2 v2.6.6 // indirect
	github.com/nats-io/nats.go v1.13.1-0.20211122170419-d7c1d78a50fc
	google.golang.org/protobuf v1.27.1 // indirect
)
